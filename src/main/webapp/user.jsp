<%--
  Created by IntelliJ IDEA.
  User: nournasrallah
  Date: 20/05/2020
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Page User</title>
</head>
<body>

    <h4>Bienvenue : ${sessionScope.sessionUtilisateur.nom} </h4>
    <h4>Voici vos informations : </h4>

    <label>Id : ${sessionScope.sessionUtilisateur.id}</label>
    </br>
    <label>Email : ${sessionScope.sessionUtilisateur.email}</label>
    </br>
    <label>Mot de Passe : ${sessionScope.sessionUtilisateur.motDePasse}</label>
    </br>
    <label>Date d'inscription : ${sessionScope.sessionUtilisateur.dateInscription}</label>
    </br>
    <a href='${pageContext.request.contextPath}/deconnexion'>
        <button formaction="">Déconnexion</button>
    </a>
</body>
</html>
