<%--
  Created by IntelliJ IDEA.
  User: nournasrallah
  Date: 20/05/2020
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Page Admin</title>
</head>
<body>
    <h4>Bienvenue : ${sessionScope.sessionUtilisateur.nom} </h4>

    <h4>Liste des utilisateurs :</h4>

        <table border="true">
            <thead>
            <tr>
                <th> Id  </th>
                <th> Nom </th>
                <th> Email </th>
                <th> Mot de passe </th>
                <th> Date d'inscription  </th>
                <th> Suppression </th>
            </tr>
            </thead>
            <!-- Table contenant les réservations-->
            <c:forEach items = "${listUtilisateurs}" var="ut" >
                <tr>
                    <td>${ut.id}</td>
                    <td>${ut.nom}</td>
                    <td>${ut.email}</td>
                    <td>${ut.motDePasse}</td>
                    <td>${ut.dateInscription}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/delete?userId=${ut.id}&admin=${sessionScope.sessionUtilisateur.id}">Supprimer</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    <br>
    <a href='${pageContext.request.contextPath}/deconnexion'>
        <button formaction="">Déconnexion</button>
    </a>
</body>
</html>
