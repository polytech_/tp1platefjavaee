package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bdd.DAOContext;
import beans.Utilisateur;
import forms.InscriptionForm;

@WebServlet(urlPatterns = "/inscription", name = "inscription")
public class Inscription extends HttpServlet {
    public static final String ATT_USER = "utilisateur";
    public static final String ATT_FORM = "form";
    public static final String ATT_RESULTAT = "resultat";

    public static final String VUE      = "/inscription.jsp";

    @Override
    public void init() throws ServletException {
        DAOContext.init( this.getServletContext() );
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page d'inscription */
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response )
            throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        InscriptionForm form = new InscriptionForm();

        /*
         * Appel au traitement et à la validation de la requête, et récupération
         * du bean en résultant
         */
        Utilisateur utilisateur = null;

        String admin = request.getParameter("admin");
        if(admin==null){
            request.setAttribute(ATT_USER, "");
        }
        else if((admin.equals("Admin"))){
            request.setAttribute(ATT_USER, "admin");
        }
        utilisateur = form.inscrireUtilisateur( request );


        String resultatInscription = form.getResultat();

        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute( ATT_FORM, form );
        request.setAttribute( ATT_USER, utilisateur);
        request.setAttribute( ATT_RESULTAT, resultatInscription);

        if(form.getErreurs().isEmpty()){
            request.getRequestDispatcher("connexion.jsp").forward(request, response);
            //response.sendRedirect("connexion.jsp");
            return;
        }

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}