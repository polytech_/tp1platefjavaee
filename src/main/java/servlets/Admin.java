package servlets;

import bdd.UtilisateurDAO;
import beans.Utilisateur;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "Admin", urlPatterns = "/admin")
public class Admin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            List<Utilisateur> listUtilisateurs = UtilisateurDAO.getAllNotAdminUsers();
            request.setAttribute("listUtilisateurs", listUtilisateurs);
            //response.sendRedirect("admin.jsp");
            request.getRequestDispatcher("admin.jsp").forward(request, response);
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
