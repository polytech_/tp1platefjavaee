package filtres;

import bdd.UtilisateurDAO;
import beans.Utilisateur;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "AdminFilter", urlPatterns = "/admin")
public class AdminFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        HttpSession session = request.getSession(false);

        if(session==null){
            response.sendRedirect("connexion.jsp");
        }

        Utilisateur utilisateur = (Utilisateur)session.getAttribute("sessionUtilisateur");

        if(utilisateur!=null && UtilisateurDAO.isValidLogin(utilisateur.getEmail(), utilisateur.getMotDePasse())
                && utilisateur.getAdmin()) {
            request.getRequestDispatcher("admin").forward(request, response);
        }
        else if(utilisateur!=null && UtilisateurDAO.isValidLogin(utilisateur.getEmail(), utilisateur.getMotDePasse())
                && !utilisateur.getAdmin()) {
            response.sendRedirect("user");
        }

    }

}
