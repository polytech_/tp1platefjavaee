package bdd;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import beans.Utilisateur;
import com.mysql.cj.protocol.Resultset;

public class UtilisateurDAO extends DAOContext {
    public static Boolean isValidLogin( String login, String password ) {
        try ( Connection connection = DriverManager.getConnection( dbURL, dbLogin, dbPassword ) ) {
            // String strSql = "SELECT * FROM T_Users WHERE login='" + login +
            // "' AND password='" + password + "'";
            String strSql = "SELECT * FROM Utilisateur WHERE email=? AND mot_de_passe=?";
            try ( PreparedStatement statement = connection.prepareStatement( strSql ) ) {
                statement.setString( 1, login );
                statement.setString( 2, password );
                try ( ResultSet resultSet = statement.executeQuery() ) {
                    if ( resultSet.next() ) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } catch ( Exception exception ) {
            throw new RuntimeException( exception );
        }
    }

    public static Boolean isValidEmail( String login ) {
        try ( Connection connection = DriverManager.getConnection( dbURL, dbLogin, dbPassword ) ) {

            String strSql = "SELECT * FROM Utilisateur WHERE email=?";
            try ( PreparedStatement statement = connection.prepareStatement( strSql ) ) {
                statement.setString( 1, login );
                try ( ResultSet resultSet = statement.executeQuery() ) {
                    if ( resultSet.next() ) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } catch ( Exception exception ) {
            throw new RuntimeException( exception );
        }
    }

    public static void creerUtilisateur( Utilisateur utilisateur ) {
        try ( Connection connection = DriverManager.getConnection( dbURL,
                dbLogin, dbPassword ) ) {

            String strSql = "INSERT INTO Utilisateur (email, mot_de_passe, nom, isadmin, date_inscription) VALUES (?, ?, ?, ?, NOW())";
            try ( PreparedStatement statement = connection.prepareStatement(
                    strSql ) ) {
                statement.setString( 1, utilisateur.getEmail() );
                statement.setString( 2, utilisateur.getMotDePasse() );
                statement.setString( 3, utilisateur.getNom() );
                statement.setBoolean( 4, utilisateur.getAdmin() );
                statement.executeUpdate();
            }
        } catch ( Exception exception ) {
            throw new RuntimeException( exception );
        }
    }

    public static Utilisateur getUtilisateur(String email, String password) {

        Utilisateur utilisateur = new Utilisateur();
        try (Connection connection = DriverManager.getConnection(dbURL,
                dbLogin, dbPassword)) {

            String strSql = "SELECT * FROM Utilisateur WHERE email=?";
            PreparedStatement preparedStatement = connection.prepareStatement(strSql);
            preparedStatement.setString(1, email);
            ResultSet resultset = preparedStatement.executeQuery();

            while (resultset.next()) {
                resultset.first();
                long id = resultset.getLong("id");
                String nom = resultset.getString("nom");
                Timestamp date = resultset.getTimestamp("date_inscription");
                Boolean isadmin = resultset.getBoolean("isadmin");

                utilisateur.setId(id);
                utilisateur.setMotDePasse(password);
                utilisateur.setEmail(email);
                utilisateur.setNom(nom);
                utilisateur.setAdmin(isadmin);
                utilisateur.setDateInscription(date);
            }

        }catch(SQLException e){
                e.printStackTrace();
        }
        return utilisateur;
    }

    public static List<Utilisateur> getAllNotAdminUsers(){

        List<Utilisateur> utilisateurList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(dbURL,
                dbLogin, dbPassword)) {

            String strSql = "SELECT * FROM Utilisateur WHERE isadmin=?";
            PreparedStatement preparedStatement = connection.prepareStatement(strSql);
            preparedStatement.setInt(1, 0);
            ResultSet resultset = preparedStatement.executeQuery();

            while (resultset.next()) {
                Utilisateur utilisateur = new Utilisateur();
                long id = resultset.getLong("id");
                String nom = resultset.getString("nom");
                Timestamp date = resultset.getTimestamp("date_inscription");
                Boolean isadmin = resultset.getBoolean("isadmin");
                String password = resultset.getString("mot_de_passe");
                String email = resultset.getString("email");

                utilisateur.setId(id);
                utilisateur.setMotDePasse(password);
                utilisateur.setEmail(email);
                utilisateur.setNom(nom);
                utilisateur.setAdmin(isadmin);
                utilisateur.setDateInscription(date);

                utilisateurList.add(utilisateur);
            }

        }catch(SQLException e){
            e.printStackTrace();
        }
        return utilisateurList;
    }

    public static void deleteUserByID(int id){

        try (Connection connection = DriverManager.getConnection(dbURL,
                dbLogin, dbPassword)) {

            String sql = "DELETE FROM Utilisateur WHERE id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}